@extends('layout')
@section('content')
@include('slider')
<h2 class="title text-center"> Liste des Restaurants</h2>
  
    
    
<div class="container">
    <h3> </h3>
    <div class="row">
        <ul class="card-list">

            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}" style="background-image: url(https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/10/attachment_74455091-e1587407791277.png?auto=format&q=60&fit=max&w=930);">
                    <img src="" alt="Restaurant 1" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}">
                    <h2>KalzBrgr</h2>
                    <p>Fast Food & Burger</p>
                </a>
            </li>
            
            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}" style="background-image: url(https://www.creads.fr/app/uploads/sites/1/2019/06/logo-oplato-terroir.png);" data-image-full="">
                    <img src="" alt="Restaurant 2" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}" >
                    <h2>Oplato</h2>
                    <p>Fast Food</p>
                </a>
            </li>
            
            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}"  style="background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3r-kByHwbjc6v1pAJiOKnLlsZw85v1QU9lA&usqp=CAU);" data-image-full="">
                    <img src="" alt="Restaurant 3" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}">
                    <h2>Asia Wok</h2>
                    <p>Cuisine Asiatique</p>
                </a>
            </li>
            
            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}"style="background-image: url(https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/10/attachment_83912442.jpg?auto=format&q=60&fit=max&w=930);" data-image-full="">
                    <img src="" alt="Restaurant 4" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}">
                    <h2>La cave</h2>
                    <p>Fin gourmet</p>
                </a>
            </li>

            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}"style="background-image: url(https://www.creads.fr/app/uploads/sites/1/2019/06/be6e707d45a9a1c49af72653c422b881-1.png);" data-image-full="">
                    <img src="" alt="Restaurant 5" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}">
                    <h2>Original Break</h2>
                    <p>Restauration Rapide</p>
                </a>
            </li>

            <li class="card">
                <a class="card-image" href="{{URL::to('/Restaurant')}}"style="background-image: url(https://99designs-blog.imgix.net/blog/wp-content/uploads/2019/10/attachment_40533723-e1587399393124.png?auto=format&q=60&fit=max&w=930);" data-image-full="">
                    <img src="" alt="Restaurant 6" />
                </a>
                <a class="card-description" href="{{URL::to('/Restaurant')}}">
                    <h2>ButterMilk</h2>
                    <p>Southern Kitchen</p>
                </a>
            </li>
            
        </ul>        
</div>
    </div>
   

@endsection